﻿// ThreadConcurrency
// Copyright (C) 2018 Dust in the Wind
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Diagnostics;
using DustInTheWind.ConsoleTools;
using DustInTheWind.ConsoleTools.Spinners;
using DustInTheWind.ThreadConcurrency.Incrementers;

namespace DustInTheWind.ThreadConcurrency
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            const int threadCount = 100;
            const int incrementCount = 1000000;

            CustomConsole.WriteLineEmphasies("Thread Concurrency");
            CustomConsole.WriteLineEmphasies(new string('=', 79));

            CustomConsole.WriteLineEmphasies("This application will increment a value from different threads.");
            CustomConsole.WriteLine("Thread count: {0}", threadCount);
            CustomConsole.WriteLine("Increment count: {0}", incrementCount);
            CustomConsole.Write("Expected Value: ");
            CustomConsole.WriteLine(ConsoleColor.DarkYellow, threadCount * incrementCount);
            CustomConsole.WriteLine();

            CustomConsole.WriteLine();
            RunUnsafe(threadCount, incrementCount);

            CustomConsole.WriteLine();
            RunWithLock(threadCount, incrementCount);

            CustomConsole.WriteLine();
            RunWithInterlocked(threadCount, incrementCount);
        }

        private static void RunUnsafe(int threadCount, int incrementCount)
        {
            UnsafeIncrementer incrementer = new UnsafeIncrementer(threadCount, incrementCount);

            const string title = "Incrementing the value without any synchronization mechanism.";
            Run(title, incrementer);
        }

        private static void RunWithLock(int threadCount, int incrementCount)
        {
            LockIncrementer incrementer = new LockIncrementer(threadCount, incrementCount);

            const string title = "Incrementing the value using lock keyword.";
            Run(title, incrementer);
        }

        private static void RunWithInterlocked(int threadCount, int incrementCount)
        {
            InterlockedIncrementer incrementer = new InterlockedIncrementer(threadCount, incrementCount);

            const string title = "Incrementing the value using Interlocked static class.";
            Run(title, incrementer);
        }

        private static void Run(string title, IIncrementer incrementer)
        {
            TimeSpan elapsedTime = MeasureTimeWithSpiner(title, incrementer.Run);

            CustomConsole.Write("Actual Value = ");
            CustomConsole.WriteLine(ConsoleColor.DarkYellow, incrementer.Value);
            CustomConsole.WriteLine("Time = {0}", elapsedTime);

            Pause.QuickDisplay();
        }

        private static TimeSpan MeasureTimeWithSpiner(string title, Action action)
        {
            using (Spinner spinner = new Spinner())
            {
                spinner.MarginTop = 1;
                spinner.Label = new InlineText
                {
                    Text = title,
                    ForegroundColor = ConsoleColor.White,
                    MarginRight = 1
                };
                spinner.DoneText = "[Done]";
                spinner.Display();

                Stopwatch stopwatch = Stopwatch.StartNew();
                action();
                return stopwatch.Elapsed;
            }
        }
    }
}